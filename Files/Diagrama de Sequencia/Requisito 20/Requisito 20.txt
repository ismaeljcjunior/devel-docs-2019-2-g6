@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Jogador ganha o jogo.
Cliente -> Cliente: Prepara o comando: playerwon < iDJogador > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Servidor: Pega a lista de vencedores e prepara para enviar para o cliente.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: playerwon < iDJogador >� winlist  List{< type >< nickname >;< points >}� em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Monta a lista de Vencedores.
Cliente -> Jogador : Notifica no batepapo quem foi o vencedor da partida.

@enduml
